# Getting Started with Spark and Hadoop

[Spark SQL Cheat Sheet](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/PySpark_SQL_Cheat_Sheet_Python.pdf)

### Useful Code Snippits
When you are writing python scripts that will be executed on the Spark cluster, all interaction with the cluster comes from a SparkSession object:

    from pyspark.sql import SparkSession
    
    spark = SparkSession.builder.appName(<your app name>).getOrCreate()
    
Some more common things you will want to do when starting a Spark script are changing the logging level from "INFO" to "ERROR", and setting up a checkpoint directory to enable checkpointing:

    spark.sparkContext.setLogLevel("ERROR")

    spark.sparkContext.setCheckpointDir('<path to directory>')

Because of how YARN is configured on the RAAS Spark cluster, changes to Spark config options must be set with command line arguments to either `pyspark` or `spark-submit`. Because of this we usually use shell scripts to run `.py` files with `spark-submit`. A basic submit script with dynamic resource allocation is below, and the full list of [Spark config options can be found here](https://spark.apache.org/docs/2.3.0/configuration.html)

    spark-submit --master yarn \
    --deploy-mode client \
    --executor-memory 20G \
    --conf spark.shuffle.service.enabled=true \
    --conf spark.dynamicAllocation.enabled=true \
    --conf spark.dynamicAllocation.minExecutors=15 \
    --executor-cores 6 \
    --driver-memory 20G $1;

### Things to Keep in Mind
- **Lazy Execution**: When you apply a transformation to a Spark dataframe, no actual computation takes place. Instead, that transformation is added to the query plan for that dataframe. When it comes time to look at or save your dataframe, only then are the actual dataframe transformations computed. This can make Spark code difficult to debug, especially if you are relying on print statements and console output. There are outdated guides on the internet that say that you can force query execution by calling `.count()` on a dataframe. This is no longer true; the way to ensure query execution is by using `.checkpoint()`.
- **Caching**: related to lazy execution, caching here refers to the way that the results of queries are saved for later use. In general, Spark tries to be smart about caching the results of query execution, so that multiple operations with the same object do not each need to re-execute the entire saved query plan. You can tell Spark to cache an object with the `.cache()` method. A balance needs to be struck with caching; more cached objects leads to increased JVM garbage collection times and more memory usage, but re-executing enitre query plans can aso be very time consuming.
- There are important differences between `.cache()` and `checkpoint()`. Cached dataframes are stored in memory (RAM) and their query plan is saved so that the dataframe can be deleted from memory and re-computed if the memory is needed. `.checkpoint()`, on the other hand, saves the dataframe  to hdfs and then removes the query plan from the dataframe. This can be useful to ensure that very expensive data transformations will not be re-computed, but any reading from or writing to a filesysystem can really slow down a Spark job.
- In general when writing Spark code we want to give as much information to the catalyst query optimizer as we can. This means chaining together many sql operations and data transformations and letting the optimizer determine the best order of operatons. To this end we want to avoid using loops and iterating over a dataframe, we instead want to think in terms of SQL operations to the whole dataset.

### Moving Data in and out of Hadoop
If there is space on your local server, the easiest way to transfer files to and from the Hadoop filesystem is with the hdfs command line tool.

    hdfs dfs -copyFromLocal <local file> <destination in hdfs>
This command simply copies a file from local storage to hdfs. This is often helpful because Spark is reads files from hdfs much faster that local files. The alternative is to read in local data with standard python code and then use `spark.createDataframe(<python df>)`, but the conversion from python/pandas to pyspark is slow and can run into memory problems.

    hdfs dfs -getMerge <local destination> <hdfs file location>
This command takes a file in hdfs and moves it to the local filesystem. Since hdfs stores files in partitions that are distributed across a clusters, if we use `-copyToLocal` we will end up with a directory containing all those partitions as seperate files. Helpfully, `-getMerge` both moves the files and merges the seperate partitions back into one file.

If you find yourself in a situation were there is not enough room on the local filesystem for your output, there are other options to move the data out of hadoop that bypass the local server.
<!---
Need to look up specifics IRS side, but you can use hdfs dfs -cat and then pipe the output into ssh and write it to disk on either mercurcy, loki, or the GPU.
-->



### Spark Dataframes
There are two main datatypes and workflows that you can use with Spark. The original datatype is the RDD, Resilient Distributed Dataset. The Dataframe datatype is part of the Spark SQL module. 

For our purposes the main difference between RDDs and Dataframes is that Dataframe operations use the SQL query planner and catalyst optimizer. This means that for most use cases, keeping all of your data as Dataframes and using Spark sql will be much faster and use less memory than using RDDs.

- [Spark SQL Getting Started Guide](https://spark.apache.org/docs/2.3.2/sql-programming-guide.html)
- [RDD Getting Started Guide](https://graphframes.github.io/graphframes/docs/_site/user-guide.html) 

In addition to Spark SQL, there are two other main Spark packages that we frequently use.

### SparkML and Graphframes
- [Spark ML guide](https://spark.apache.org/docs/2.3.2/ml-guide.html) - This contains various ml models, but more importantly it has an array of useful data processing/cleaning tools.
- [Spark GraphFrames guide](https://graphframes.github.io/graphframes/docs/_site/user-guide.html) - A common Spark add-on to analyze graph structured data using Spark SQL dataframes. [Graphx](https://spark.apache.org/docs/2.3.2/graphx-programming-guide.html) is a built in graph analysis package for Spark, but graphx is does not have pySpark support, so it can only be used with scala. In addition, since graphx is based on RDD's instead of dataframes, if GraphFrames can be used it *should* be more performant.

### Tuning and Troubleshooting

- [Spark 2.3 Tuning](https://Spark.apache.org/docs/2.3.2/tuning.html)
- [Spark 3.0 Tuning](https://spark.apache.org/docs/3.0.0/tuning.html)


In the RAAS Spark environment most Spark code is run with Spark 2.3. The Spark 3.0 files are available, but using these can sometimes cause problems. The main reasons you might want to use spark 3 are for the improved python error messages, for the adaptive query execution. These guides mostly focus on Spark 2.3, but versions for Spark 3.0 are usually also available.
